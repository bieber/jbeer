/**   
 * @Title: AppConfig.java
 * @Package org.jbeer.sample.config
 * @author Bieber
 * @date 2014年5月31日 下午1:33:09
 * @version V1.0   
 */

package org.jbeer.sample.config;

import com.jbeer.framework.config.*;
import com.jbeer.framework.ws.config.WSConfig;
import org.jbeer.sample.bean.service.SimpleWebservice;
import org.jbeer.sample.ws.AuthDataProviderImpl;
import org.jbeer.sample.ws.WSSecurityValidate;

/**
 * <p>
 * 类功能说明:TODO
 * </p>
 * <p>
 * 类修改者 修改日期
 * </p>
 * <p>
 * 修改说明
 * </p>
 * <p>
 * Title: AppConfig.java
 * </p>
 * 
 * @author Bieber <a
 *         mailto="bieber.jbeer@hotmail.com">bieber.jbeer@hotmail.com</a>
 * @date 2014年5月31日 下午1:33:09
 * @version V1.0
 */

public class AppConfig implements Configurator {


	@Override
	public void configContext(JBeerConfig config) {
		config.setApplicationEncode("UTF-8");
	}


	@Override
	public void configAop(AopConfig config) {
	}


	@Override
	public void configDB(DBConfig config) {
		config.setDatasource("${jdbc_initSize}", "${jdbc_maxSize}",
				"${jdbc_minSize}", "${jdbc_timeout}", "${jdbc_userName}",
				"${jdbc_password}", "${jdbc_url}", "${jdbc_driver}");
	}


	@Override
	public void configWeb(WebConfig config) {
		config.setViewPrefix("WEB-INF/pages");
		config.setViewSuffix(".tpl");
		config.isSingletonMode(true);
	}


	@Override
	public void configIOC(IOCConfig config) {
	}


	@Override
	public void configIN18(IN18Config config) {
		config.setBaseName("in18_message");
	}


	@Override
	public void configProperties(PropertiesConfig config) {
		config.setPropertiesPath("*_test.properties");
		config.setPropertiesPath("conf/*.properties");
	}

	@Override
	public void pluginConfig(PluginConfigHandler handler) {
		//handler.registerPluginConfig(new MyBatisConfig());
		WSConfig config = new WSConfig();
		config.setInterceptorLogger(true);
		config.setSecurityWS(true);
		config.setAuthorizationValidate(new WSSecurityValidate());
		config.registeWSClient(SimpleWebservice.class, "${host}${simple_ws_url}");
		config.setAuthProvidor(new AuthDataProviderImpl());
		handler.registerPluginConfig(config);
	}

}
