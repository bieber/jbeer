package org.jbeer.sample.controller;

import com.jbeer.framework.annotation.Action;
import com.jbeer.framework.annotation.Controller;
import com.jbeer.framework.web.ModelAndView;

/**
 * Created by bieber on 14-11-23.
 */
@Controller
public class IndexController {

    @Action(urlPatterns = "freemarker.htm")
    public ModelAndView index(){
        ModelAndView.PageModelAndView modelAndView = ModelAndView.createModelAndView();
        modelAndView.setView("index");
        modelAndView.setDataMap("userName","bieber");
        return modelAndView;
    }
}
