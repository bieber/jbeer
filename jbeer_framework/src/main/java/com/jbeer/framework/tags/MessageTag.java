/**
 * 
 */
package com.jbeer.framework.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import com.jbeer.framework.exception.MessageException;
import com.jbeer.framework.logging.Log;
import com.jbeer.framework.message.MessageUtils;
import com.jbeer.framework.utils.LoggerUtil;
import com.jbeer.framework.utils.StringUtils;

/**
 * @author bieber
 * 消息标签
 */
public class MessageTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4978007335283815634L;

	private PageContext context;
	
	private String messageKey;
	
	private String defaultMessage;
	
	private static final Log logger = LoggerUtil.generateLogger(MessageTag.class);
	
	@Override
	public int doStartTag() throws JspException {
		JspWriter writer = context.getOut();
		try {
			if(StringUtils.isEmpty(messageKey)){
				writer.print("not found message key");
				return SKIP_BODY;
			}
			String message = MessageUtils.getMessage(messageKey, context.getRequest().getLocale());
			if(!StringUtils.isEmpty(message)){
				writer.print(message);
			}else if(!StringUtils.isEmpty(defaultMessage)){
				writer.print(defaultMessage);
			}
		} catch (MessageException e) {
			logger.debug("failed to get message from contain  fro key "+messageKey, e);
		} catch (IOException e) {
			 logger.debug("failed to rending message content for jsp page", e);
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
	@Override
	public void setPageContext(PageContext pageContext) {
		context=pageContext;
	}

	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}

	
}
