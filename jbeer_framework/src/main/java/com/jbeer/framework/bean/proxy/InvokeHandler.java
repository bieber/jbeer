/**   
* @Title: MethodHandler.java
* @Package com.jbeer.framework.ioc.aop
* @author Bieber
* @date 2014-5-18 下午2:03:14
* @version V1.0   
*/

package com.jbeer.framework.bean.proxy;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * <p>类功能说明:调用的劫持者</p>
 * <p>类修改者	    修改日期</p>
 * <p>修改说明</p>
 * <p>Title: MethodHandler.java</p>
 * @author Bieber <a mailto="bieber.jbeer@hotmail.com">bieber.jbeer@hotmail.com</a>
 * @date 2014-5-18 下午2:03:14
 * @version V1.0
 */

public interface InvokeHandler {

    public Object invoke() throws Throwable;

    public Object getProxyObject();

    public Method getInvokeMethod();

    public Class<?> getTargetClass();

    public Object[] getArgs();

    public boolean isInvoked();

}
