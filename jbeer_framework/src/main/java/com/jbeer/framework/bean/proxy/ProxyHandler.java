/**   
* @Title: BeanProxy.java
* @Package com.jbeer.framework.ioc.proxy
* @author Bieber
* @date 2014年5月19日 上午10:18:19
* @version V1.0   
*/

package com.jbeer.framework.bean.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
* <p>类功能说明:Bean代理的抽象类,定义代理类的模板，整合了JDK代理和cglib代理</p>
* <p>类修改者	    修改日期</p>
* <p>修改说明</p>
* <p>Title: BeanProxy.java</p>
* @author Bieber <a mailto="bieber.jbeer@hotmail.com">bieber.jbeer@hotmail.com</a>
* @date 2014年5月19日 上午10:18:19
* @version V1.0
*/

public final class ProxyHandler implements InvocationHandler,MethodInterceptor{

    protected ProxyTargetProcessor processor;
    protected Class<?> targetClass;

	/**
	* <p>Title: </p>
	* <p>Description: </p>
	* @param processor
	* @param targetClass
	*/
	public ProxyHandler(ProxyTargetProcessor processor, Class<?> targetClass) {
		this.processor = processor;
        this.targetClass = targetClass;
	}



    /* (non-Javadoc)
     * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
     */
    public final Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        InvokeHandler handler = new JDKInvokeHandler(method,targetClass,args,proxy);
        try{
            return processor.invokeTarget(handler);
        }catch(InvocationTargetException e){
        	throw e.getTargetException();
        }
    }

    /* (non-Javadoc)
     * @see net.sf.cglib.proxy.MethodInterceptor#intercept(java.lang.Object, java.lang.reflect.Method, java.lang.Object[], net.sf.cglib.proxy.MethodProxy)
     */
    public final Object intercept(Object proxy, Method method, Object[] args, MethodProxy handler) throws Throwable{
        InvokeHandler invokeHandler = new CGLIBInvokeHandler(method,targetClass,args,proxy,handler);
        try{
            return processor.invokeTarget(invokeHandler);
        }catch(InvocationTargetException e){
        	throw e.getTargetException();
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((targetClass == null) ? 0 : targetClass.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (this.getClass() != obj.getClass())
            return false;
        ProxyHandler other = (ProxyHandler) obj;
        if (targetClass == null) {
            if (other.targetClass != null)
                return false;
        } else if (!targetClass.equals(other.targetClass))
            return false;
        return true;
    }

    
}
