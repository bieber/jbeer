package com.jbeer.framework.bean.proxy;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created by bieber on 2014/12/1.
 */
public class CGLIBInvokeHandler implements InvokeHandler {

    private Method invokeMethod;

    private Class<?> targetClass;

    private Object[] args;

    private Object proxyObject;

    private volatile boolean isInvoked=false;

    private MethodProxy methodProxy;


    public CGLIBInvokeHandler(Method invokeMethod, Class<?> targetClass, Object[] args, Object proxyObject, MethodProxy methodProxy) {
        this.invokeMethod = invokeMethod;
        this.targetClass = targetClass;
        this.args = args;
        this.proxyObject = proxyObject;
        this.methodProxy = methodProxy;
    }

    @Override
    public Object invoke() throws Throwable {
        if(!isInvoked()){
            isInvoked=true;
            return methodProxy.invokeSuper(proxyObject,args);
        }
        return null;
    }

    @Override
    public Object getProxyObject() {
        return proxyObject;
    }

    @Override
    public Method getInvokeMethod() {
        return invokeMethod;
    }

    @Override
    public Class<?> getTargetClass() {
        return targetClass;
    }

    @Override
    public Object[] getArgs() {
        return args;
    }

    @Override
    public boolean isInvoked() {
        return isInvoked;
    }
}
