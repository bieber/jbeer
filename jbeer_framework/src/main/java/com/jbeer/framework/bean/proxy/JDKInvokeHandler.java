package com.jbeer.framework.bean.proxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by bieber on 2014/12/1.
 */
public class JDKInvokeHandler implements InvokeHandler{


    private Method invokeMethod;

    private Class<?> targetClass;

    private Object[] args;

    private Object proxyObject;

    private volatile boolean isInvoked=false;

    public JDKInvokeHandler(Method invokeMethod, Class<?> targetClass, Object[] args, Object proxyObject) {
        this.invokeMethod = invokeMethod;
        this.targetClass = targetClass;
        this.args = args;
        this.proxyObject = proxyObject;
    }

    @Override
    public Object invoke() throws InvocationTargetException, IllegalAccessException {
        if(!isInvoked()) {
            isInvoked=true;
            return invokeMethod.invoke(proxyObject,args);
        }
        throw new IllegalStateException(targetClass.getName()+"->"+invokeMethod.getName()+" had invoked");
    }

    @Override
    public Object getProxyObject() {
        return proxyObject;
    }

    @Override
    public Method getInvokeMethod() {
        return invokeMethod;
    }

    @Override
    public Class<?> getTargetClass() {
        return targetClass;
    }

    @Override
    public Object[] getArgs() {
        return this.args;
    }

    @Override
    public boolean isInvoked() {
        return isInvoked;
    }
}
