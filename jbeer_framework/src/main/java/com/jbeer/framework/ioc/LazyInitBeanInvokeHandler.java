package com.jbeer.framework.ioc;

import com.jbeer.framework.bean.proxy.InvokeHandler;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**                                                                           |——如果目标Bean是经过AOP代理的，则调用methodProxy的invokeSuper方法,执行AOP的流程
 * 延迟初始化的依赖属性是执行流程：LazyInitBeanInvokeHandler->IOCContainer.getBean->
 *                                                                            |——如果目标Bean没有经过AOP代理，则调用methodProxy的invoke直接调用实体即可。
 * Created by bieber on 2014/12/1.
 */
public class LazyInitBeanInvokeHandler implements MethodInterceptor {

    private String beanName;

    public LazyInitBeanInvokeHandler(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        Object beanInstance = JBeerIOCContainerContext.getBeanById(beanName);
        Class<?> beanClass = beanInstance.getClass();
        return methodProxy.invokeSuper(beanInstance,objects);
    }
}
