/**   
* @Title: FreemarkeViewer.java
* @Package com.jbeer.framework.web.viewer
* @author Bieber
* @date 2014年6月17日 上午9:57:50
* @version V1.0   
*/

package com.jbeer.framework.web.viewer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.jbeer.framework.JBeer;
import com.jbeer.framework.JBeerWeb;
import com.jbeer.framework.annotation.RefBean;
import com.jbeer.framework.exception.JBeerException;
import com.jbeer.framework.logging.Log;
import com.jbeer.framework.plugin.Plugin;
import com.jbeer.framework.utils.LoggerUtil;

import com.jbeer.framework.utils.StringUtils;
import com.jbeer.framework.web.JBeerWebContext;
import com.jbeer.framework.web.viewer.config.FreemarkerConfig;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateException;

/**
* <p>类功能说明:freemarke视图插件</p>
* <p>类修改者	    修改日期</p>
* <p>修改说明</p>
* <p>Title: FreemarkeViewer.java</p>
* @author Bieber <a mailto="bieber.jbeer@hotmail.com">bieber.jbeer@hotmail.com</a>
* @date 2014年6月17日 上午9:57:50
* @version V1.0
*/

public class FreemarkerViewer implements Plugin {

    private Configuration freemarkerConfiguration=null;

    @RefBean(FreemarkerConfig.PLUGIN_CONFIG_NAMESPACE)
    private FreemarkerConfig freemarkerConfig;


    private boolean isCacheTemplate;
    
    private static final Log logger = LoggerUtil.generateLogger(FreemarkerViewer.class);
    
    public Configuration getFreemarkerConfig(){
        return freemarkerConfiguration;
    }
    /* (non-Javadoc)
     * @see com.jbeer.framework.plugin.Plugin#initialize()
     */
    public void initialize() throws JBeerException, IOException {
        isCacheTemplate=freemarkerConfig.isCacheTemplate();
        if(freemarkerConfig.getConfiguration()==null) {
            freemarkerConfiguration = new Configuration();
            freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        }else{
            freemarkerConfiguration = freemarkerConfig.getConfiguration();
        }

        if(!StringUtils.isEmpty(freemarkerConfig.getFilePath())){
            Properties properties = new Properties();
            InputStream in = JBeer.getCurrentClassLoader().getResourceAsStream(freemarkerConfig.getFilePath());
            try {
                properties.load(in);
                freemarkerConfiguration.setSettings(properties);
            } catch (IOException e) {
                throw new IllegalArgumentException("failed to freemarker config file load!");
            } catch (TemplateException e) {
                throw new JBeerException("failed to setting freemarker config",e);
            }
        }
        if(logger.isDebugEnabled()){
            logger.debug("initialize freemarker viewer....");
        }
        try {
            if(!StringUtils.isEmpty(freemarkerConfig.getDefaultEncoding())){
                freemarkerConfiguration.setDefaultEncoding(freemarkerConfig.getDefaultEncoding());
            }else{
                freemarkerConfiguration.setDefaultEncoding(JBeer.getApplicationEncode());
            }
            if(!StringUtils.isEmpty(freemarkerConfig.getClassPathDirectoryForTemplate())){
                freemarkerConfiguration.setServletContextForTemplateLoading(JBeerWebContext.getServletContext(),freemarkerConfig.getClassPathDirectoryForTemplate());
            }else if(!StringUtils.isEmpty(freemarkerConfig.getDirectoryForTemplate())){
                freemarkerConfiguration.setDirectoryForTemplateLoading(new File(freemarkerConfig.getDirectoryForTemplate()));
            }else{
                freemarkerConfiguration.setServletContextForTemplateLoading(JBeerWebContext.getServletContext(),JBeerWeb.getViewPrefix());
            }

        } catch (IOException e) {
            throw new JBeerException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.jbeer.framework.plugin.Plugin#destroy()
     */
    public void destroy() throws JBeerException {
        if(logger.isDebugEnabled()){
            logger.debug("destroy freemarker viewer....");
        }
        if(freemarkerConfiguration!=null){
            freemarkerConfiguration.clearTemplateCache();
            freemarkerConfiguration=null;
        }
    }

    public boolean isCacheTemplate() {
        return isCacheTemplate;
    }
}
