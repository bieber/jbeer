package com.jbeer.framework.web.viewer.config;

import com.jbeer.framework.config.PluginConfig;
import freemarker.template.Configuration;

import java.net.URL;

/**
 * Created by bieber on 14-11-23.
 */
public class FreemarkerConfig implements PluginConfig {

    public static final String PLUGIN_CONFIG_NAMESPACE="freemarker_config_namespace";

    private Configuration configuration;


    private String filePath;

    private String defaultEncoding;

    private String directoryForTemplate;

    private String classPathDirectoryForTemplate;

    private boolean isCacheTemplate=false;

    public String pluginNamespace() {
        return PLUGIN_CONFIG_NAMESPACE;
    }

    public void setFreemarkerConfiguration(Configuration configuration){
        checkArgument(configuration);
        this.configuration = configuration;
    }

    /**
     * 配置freemarker信息的properties文件路径
     * @param filePath
     */
    public void setFreemarkerConfigFile(String filePath){
        checkArgument(filePath);
        if(!filePath.endsWith("properties")){
            throw new IllegalArgumentException("freemarker config file must be *.properties file");
        }
        URL url = Thread.currentThread().getContextClassLoader().getResource(filePath);
        if(url==null){
            throw new IllegalArgumentException(filePath+" is not exist!");
        }

        this.filePath = filePath;
    }

    public void setDefaultEncoding(String encoding){
        checkArgument(encoding);
        this.defaultEncoding = encoding;
    }

    /**
     * 该方法设置的时模板目录，是文件系统的绝对路径
     * @param directoryForTemplate
     */
    public void setFileSystemtDirectoryForTemplate(String directoryForTemplate){
        checkArgument(directoryForTemplate);
        this.directoryForTemplate = directoryForTemplate;
    }

    /**
     *该方法是设置CLASS路径下面的模板位置，如果未设置该值，并且未设置{@link #setFileSystemtDirectoryForTemplate(String)}，则使用{@link com.jbeer.framework.JBeerWeb#getViewPrefix()}  }
     * @param classPathDirectoryForTemplate
     */
    public void setClassPathDirectoryForTemplate(String classPathDirectoryForTemplate){
        checkArgument(classPathDirectoryForTemplate);
        this.classPathDirectoryForTemplate =classPathDirectoryForTemplate;
    }



    private void checkArgument(Object object){
        if(object==null){
            throw new IllegalArgumentException("argument is null,please check!");
        }
    }

    public boolean isCacheTemplate() {
        return isCacheTemplate;
    }

    public void setCacheTemplate(boolean isCacheTemplate) {
        this.isCacheTemplate = isCacheTemplate;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getDefaultEncoding() {
        return defaultEncoding;
    }

    public String getDirectoryForTemplate() {
        return directoryForTemplate;
    }

    public String getClassPathDirectoryForTemplate() {
        return classPathDirectoryForTemplate;
    }
}
